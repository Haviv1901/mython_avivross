#include "String.h"



// ctors
String::String(string value) : Sequence(false)
{
	_value = value;
}
String::String(string value, bool isTemp) : Sequence(isTemp)
{
	_value = value;
}

void String::setValue(string value)
{
	_value = value;
}

// consts
bool String::isPrintable() const
{
	return true; // String is printable
}

std::string String::toString() const
{
	return "String";
}

string String::getValue() const
{
	return _value;
}