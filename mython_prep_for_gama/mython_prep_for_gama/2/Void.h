#pragma once
#include "../1/Type.h"


class Void : public Type
{

public:
	// ctors
	Void();
	Void(bool isTemp);

	// consts
	bool isPrintable() const override;
	std::string toString() const override;


private:


};