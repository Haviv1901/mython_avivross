#include "Void.h"


// ctors
Void::Void() : Type(false)
{
}
Void::Void(bool isTemp) : Type(isTemp)
{
}

// consts
bool Void::isPrintable() const 
{
	return false; // void is not printable
}

std::string Void::toString() const
{
	return "Void";
}