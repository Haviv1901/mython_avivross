#pragma once
#include "../1/Type.h"


class Integer : public Type
{

public:
	// ctors
	Integer(int value);
	Integer(int value, bool isTemp);

	void setValue(bool value);

	// consts
	bool isPrintable() const override;
	std::string toString() const override;
	bool getValue() const;


private:

	bool _value;

};