#pragma once
#include <string>

#include "Sequence.h"

using std::string;


class String : public Sequence
{

public:
	// ctors
	String(string value);
	String(string value, bool isTemp);

	void setValue(string value);

	// consts
	bool isPrintable() const override;
	std::string toString() const override;
	string getValue() const;


private:

	string _value; 

};