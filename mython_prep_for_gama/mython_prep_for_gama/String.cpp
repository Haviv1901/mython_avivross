#include "String.h"

//dotr
String::~String()
{

}

// ctors
String::String(bool value) : Sequence(false)
{
	_value = value;
}
String::String(bool value, bool isTemp) : Sequence(isTemp)
{
	_value = value;
}

void String::setValue(bool value)
{
	_value = value;
}

// consts
bool String::isPrintable() const
{
	return true; // String is printable
}

std::string String::toString() const
{
	return "String";
}

bool String::getValue() const
{
	return _value;
}