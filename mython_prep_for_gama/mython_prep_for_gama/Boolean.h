#pragma once
#include "../1/Type.h"


class Boolean : public Type
{

public:
	// ctors
	Boolean(bool value);
	Boolean(bool value, bool isTemp);

	void setValue(bool value);

	// consts
	bool isPrintable() const override;
	std::string toString() const override;
	bool getValue() const;


private:

	bool _value;

};