#pragma once
#include "../1/Type.h"


class Void : public Type
{

public:
	// ctors
	Void(bool value);
	Void(bool value, bool isTemp);
	~Void() override;

	void setValue(bool value);

	// consts
	bool isPrintable() const override;
	std::string toString() const override;
	bool getValue() const;


private:

	bool _value;

};