#pragma once
#include "../1/Type.h"


class Integer : public Type
{

public:
	// ctors
	Integer(bool value);
	Integer(bool value, bool isTemp);
	~Integer() override;

	void setValue(bool value);

	// consts
	bool isPrintable() const override;
	std::string toString() const override;
	bool getValue() const;


private:

	bool _value;

};