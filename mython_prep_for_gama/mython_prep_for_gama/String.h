#pragma once
#include <string>

#include "Sequence.h"


class String : public Sequence
{

public:
	// ctors
	String(bool value);
	String(bool value, bool isTemp);
	~String() override;

	void setValue(bool value);

	// consts
	bool isPrintable() const override;
	std::string toString() const override;
	bool getValue() const;


private:

	bool _value;

};