#include "Integer.h"


//dotr
Integer::~Integer()
{

}

// ctors
Integer::Integer(bool value) : Type(false)
{
	_value = value;
}
Integer::Integer(bool value, bool isTemp) : Type(isTemp)
{
	_value = value;
}

void Integer::setValue(bool value)
{
	_value = value;
}

// consts
bool Integer::isPrintable() const
{
	return true; // Integer is printable
}

std::string Integer::toString() const
{
	return "Integer";
}

bool Integer::getValue() const
{
	return _value;
}