#include "Void.h"


//dotr
Void::~Void()
{
	
}


// ctors
Void::Void(bool value) : Type(false)
{
	_value = value;
}
Void::Void(bool value, bool isTemp) : Type(isTemp)
{
	_value = value;
}

void Void::setValue(bool value)
{
	_value = value;
}

// consts
bool Void::isPrintable() const 
{
	return false; // void is not printable
}

std::string Void::toString() const
{
	return "Void";
}

bool Void::getValue() const
{
	return _value;
}