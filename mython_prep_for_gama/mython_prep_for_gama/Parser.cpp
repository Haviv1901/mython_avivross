#include "../2/Parser.h"
#include <iostream>
#include "IndentationException.h"

using std::cout;
using std::endl;


Type* Parser::parseString(std::string str)
{
	if (str.length() == 0)
	{
		cout << endl;
		return nullptr;
	}

	if(!checkForIndent(str))
	{
		throw IndentationException();
	}

	Type* temp = getType(str);


	return nullptr;
}

bool Parser::checkForIndent(const std::string& str)
{
	if(str[0] == '\t' or str[0] == ' ')
	{
		return true;
	}
	return false;
}



Type* Parser::getType(std::string& str)
{
	Helper::trim(str);
	return nullptr;
}
