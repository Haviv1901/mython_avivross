#pragma once
#include <string>



class Type
{

public:
	Type(const bool& isTemp);
	bool getIsTemp() const;


	void setIsTemp(const bool& isTemp);

	virtual bool isPrintable() const = 0;
	virtual std::string toString() const = 0;

protected:
	bool _isTemp;

};
