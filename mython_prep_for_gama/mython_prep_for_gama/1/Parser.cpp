#include "Parser.h"
#include <iostream>
#include "IndentationException.h"

#include "../2/Integer.h"
#include "../2/Boolean.h"
#include "../2/String.h"
#include "../2/Void.h"



using std::cout;
using std::endl;

Type* Parser::parseString(std::string str)
{
	if (str.length() == 0)
	{
		cout << endl;
		return nullptr;
	}

	if(!checkForIndent(str))
	{
		throw IndentationException();
	}

	Type* type = getType(str);



	return nullptr;
}

Type* Parser::getType(std::string& str)
{
	Helper::trim(str);

	if(Helper::isInteger(str))
	{
		DEBUG("Int");
		return new Integer(std::stoi(str), true);
		
	}
	if(Helper::isBoolean(str))
	{
		DEBUG("Bool");
		return new Boolean(str == "True", true);
	}
	if(Helper::isString(str))
	{
		DEBUG("String");
		return new String(str, true);
	}

	return nullptr;
	
}

bool Parser::checkForIndent(const std::string& str)
{
	if(str[0] == '\t' or str[0] == ' ')
	{
		return true;
	}
	return false;
}

