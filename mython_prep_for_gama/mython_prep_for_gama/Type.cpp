#include "Type.h"


Type::Type(const bool& isTemp)
{
	_isTemp = isTemp;
}
bool Type::getIsTemp() const
{
	return _isTemp;
}

void Type::setIsTemp(const bool& isTemp)
{
	_isTemp = isTemp;
}