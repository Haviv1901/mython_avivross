#pragma once
#include "Sequence.h"

#include <vector>

#include "../1/Type.h"


class Sequence : public Type
{

public:

	Sequence(const bool& isTemp);
	virtual ~Sequence() = 0;

protected:


};