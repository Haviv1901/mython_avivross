#include "Boolean.h"



// ctors
Boolean::Boolean(bool value) : Type(false)
{
	_value = value;
}
Boolean::Boolean(bool value, bool isTemp) : Type(isTemp)
{
	_value = value;
}

void Boolean::setValue(bool value)
{
	_value = value;
}

// consts
bool Boolean::isPrintable() const
{
	return true; // Boolean is printable
}

std::string Boolean::toString() const
{
	return "Boolean";
}

bool Boolean::getValue() const
{
	return _value;
}